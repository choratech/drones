package com.drone.transport.repo;
import org.springframework.data.jpa.repository.JpaRepository;

import com.drone.transport.models.*;

public interface DroneRepo extends JpaRepository<Drone, Long>{

}

