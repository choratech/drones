package com.drone.transport.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drone.transport.models.Loads;

public interface LoadsRepo extends JpaRepository<Loads, Long>{

}
