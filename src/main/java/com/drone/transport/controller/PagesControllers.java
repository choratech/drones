package com.drone.transport.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.drone.transport.models.Medication;
import com.drone.transport.repo.MedicationRepo;


//THIS CLASS IS NOT USED.********************

@Controller
public class PagesControllers {

	@Autowired
	private MedicationRepo medicationRepo;
	
	 @GetMapping("/view_medication_picture/{id}")
		public String viewPic(@PathVariable long id, Model model) {
		Medication medication = medicationRepo.findById(id).get();
		 String imgName = medication.getCode()+".jpg"; // use the id of the medication as also the image file name for easy correlation
		 model.addAttribute("fileName", "medication_pictures/"+imgName);
			return "imgtemplate";
      }
      
	
	@GetMapping("/abcde")
	public String abc() {
	
		return "abc";
    }
	
}
