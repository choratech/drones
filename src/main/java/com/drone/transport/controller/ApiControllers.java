package com.drone.transport.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.drone.transport.models.Medication;
import com.drone.transport.repo.DroneRepo;
import com.drone.transport.repo.LoadsRepo;
import com.drone.transport.repo.MedicationRepo;
import com.drone.transport.upload.FileUploadUtil;
import com.drone.transport.models.Drone;
import com.drone.transport.models.State;
import com.drone.transport.models.Model;
import com.drone.transport.models.Loads;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class ApiControllers {
	
	//https://www.baeldung.com/spring-boot-start
	//https://www.baeldung.com/spring-boot-jsp
	//https://www.baeldung.com/
	//https://spring.io/guides/gs/serving-web-content/
	//https://www.tutorialspoint.com/spring/spring_page_redirection_example.htm
	
	@Autowired
	private DroneRepo droneRepo;

	@Autowired
	private MedicationRepo medicationRepo;
	
	@Autowired
	private LoadsRepo loadsRepo;
	
	 // creating a logger
     Logger logger = LoggerFactory.getLogger(ApiControllers.class);
     
     //logging.level.root=ERROR    --- logging settings for application.properties file
     //logging.level.com.drone.transport=INFO
    
	@GetMapping(value = "/") // home page 
	public String getPage() {
		/* Medication m = new Medication();
		m.setCode("78788**65577*88899-");
		m.setName("9438438jdsnds**()))))fofkk");
		
		Drone d =new Drone();
		d.setSerialNumber("12345678901173995959008");
		
		//return "Welcome";
		return m.getCode() + "<br/>" + m.getName() + "<br/>"+ d.getSerialNumber();
		*/
		return "<h1>Welcome to Drone Transport Application</h1><h3>We transport Medications to areas with difficult access.</h3>";
	}
	


    @GetMapping("/log_app_process") 
    public String logApplicationProcess()
    {
    	//logging.level.root=INFO
    	//logging.level.root=ERROR
    	//logging.level.com.drone.transport=INFO
    	
    	//LocalDateTime ldt = LocalDateTime.now();
		//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS");
		//System.out.println(ldt.format(formatter) + " Started thread " + name);
    	
        // Logging various log level messages
        //logger.trace("Log level: TRACE");
        logger.info("Log level: INFO");
        //logger.debug("Log level: DEBUG");
        //logger.error("Log level: ERROR");
        //logger.warn("Log level: WARN");
  
        return "Hi! You can check the output in the application logs";
    }

	
	
	@PostMapping(value = "/register_drone") //register a drone
	public List<String> registerDrone(@RequestBody Drone drone) {
		List<String> response = new ArrayList<>();
		
		int noOfDrones = (int) droneRepo.count(); //noOfDrones is number of rows in drone's table
		
		if(noOfDrones < 10) {
			char[] c = drone.getSerialNumber().toCharArray();
			
			if(c.length > 100) {
				response.add("Drone not registered - serial number cannot exceed 100 Characters");
				return response;
			}
			else {
				boolean duplicateSerialNo = false;
				List<Drone> drones = droneRepo.findAll();
				for(Drone d : drones) {
					if(drone.getSerialNumber().equals(d.getSerialNumber())) {
						duplicateSerialNo  = true;
						break;
					}
				}
				if(duplicateSerialNo == true) {
					response.add("Sorry! you cannot have duplicate serial numbers for Drones");
					return response;
				}
			}
			if(drone.getWeightLimit() > 500) {
				response.add("Drone not registered - Weight capacity cannot exceed 500grams");
				return response;
			}
			
			
                // Lightweight, Middleweight, Cruiserweight, Heavyweight;		
			    //String model = drone.getModel().name();	
				if((drone.getModel() == Model.Lightweight) || (drone.getModel() == Model.Middleweight) || (drone.getModel() == Model.Cruiserweight) || (drone.getModel() == Model.Heavyweight)){		
				}
				else{
					response.add("Drone not registered - Drone model can only be one of the following; Lightweight, Middleweight, Cruiserweight, Heavyweight;");
		     	    return response;
				}
			
				//IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;	
				//String state = drone.getState().name();
				if((drone.getState() == State.IDLE) || (drone.getState() == State.LOADING)  || (drone.getState() == State.LOADED)  || (drone.getState() == State.DELIVERING)  || (drone.getState() == State.DELIVERED)  || (drone.getState() == State.RETURNING)){	
				}
				else{ 
					response.add("Drone not registered - Drone state can only be one of the following; IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;");
					return response;
				}
			
		
			if(drone.getBattery() > 100) {
				drone.setBattery(100);
			}
			else if(drone.getBattery() < 25){
				drone.setState(State.IDLE);
			}
			
			droneRepo.save(drone);
			response.add("Successfully registered drone");
			return response;
		}
		else {
		response.add("Drone cannot be registered, maximum number of drones cannot exceed 10");
		return response;
		}
		
	}
	
	
	
	@PostMapping(value = "/load_medication") //create and load a medication on the fly
	public List<String> loadMedication(@RequestBody Medication medication) throws IOException {
		//String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		//String uniqueFileName = FileUploadUtil.saveFile(fileName, multipartFile); // the file is the picture of the medication case
		//long size = multipartFile.getSize();
		//String downLoadUri= "/downloadFile/"+ fileCode;
		
		List<String> response = new ArrayList<>();
		String code = medication.getCode();
		boolean check = false;
		char[] codeChar = code.toCharArray();
		for (int i = 0; i < codeChar.length; i++) {
			for (char s : Medication.codeConstraint) {
				if (codeChar[i] == s) {
					check = true;		
				}
			}
			if(check == false) {
				//break;
				response.add(code + " is an invalid code - only upper case letters, underscore and numbers are allowed");
				return response;
			}
			else {
				check = false;
			}
		}
		
		
		String name = medication.getName();
	    check = false;
		char[] nameChar = name.toCharArray();
		for (int i = 0; i < nameChar.length; i++) {
			for (char s : Medication.nameConstraint) {
				if (nameChar[i] == s) {
					check = true;
				}
			}
			if (check == false) {
				response.add( name + " is an invalid name - only letters, numbers, ‘-‘, ‘_’ are allowed");
				return response;
				//break;
			}
			else {
				check = false;
			}
		}
		//if (check == false) {
		//	return name + " Invalid name - only letters, numbers, ‘-‘, ‘_’ are allowed";			
		//}
		
		
		
		String message= "Cannot Load, No Drone is in the LOADING or LOADED State now"; //place holder message
		List<Drone> list = droneRepo.findAll();
		for(Drone dr : list) {
			if((dr.getState() == State.LOADING) || (dr.getState() == State.LOADED)) { // as you cannot load a drone when its in the DELIVERED, DELIVERING OR RETURNING State
			
				
				int sumUpLoadedMedication = 0;
				if((dr.getMedicalLoadIds() != null) && (!dr.getMedicalLoadIds().isEmpty())) {
				String[] loadings = dr.getMedicalLoadIds().split(","); // get any previous saved medical ids (that represent medications) from the Drone, and sumup their individual weights 
				for(int i =0; i< (loadings.length) ; i++) {
					if(!loadings[i].isEmpty()) {
						try {
				          	Medication medic = medicationRepo.findById(Long.valueOf(loadings[i])).get();
				          	sumUpLoadedMedication += medic.getWeight();
						}
						catch(Exception e) {
							continue;
						}
					
					}
					else {
						continue; // avoid any error due to empty strings
					}
				 }
				}
				else {
					dr.setMedicalLoadIds("");
				}
				
				if(dr.getWeightLimit() >= (sumUpLoadedMedication + medication.getWeight())) { // check if the drone can carry the new medication, along with existing medication items (if any)
					//if(!uniqueFileName.isEmpty()) {
					//	medication.setImage(uniqueFileName); //Save the path to the medication picture in database
					//}
					//upload the medication picture to medication_pictures directory on the server
					//String randomName = RandomStringUtils.randomAlphanumeric(8);
					String imgName = (String.valueOf(medication.getCode()))+".jpg"; // use the id of the medication as also the image file name for easy correlation
					Path uploadDirectory = Paths.get("src/main/resources/static/medication_pic");
					
					/*
					 * String partSeparator = ","; if (data.contains(partSeparator)) { 
					 * String encodedImg = data.split(partSeparator)[1]; 
					 * byte[] decodedImg = Base64.getDecoder().decode(encodedImg.getBytes(StandardCharsets.UTF_8)); 
					 * Path destinationFile = Paths.get("/path/to/imageDir", "myImage.png");
					 * Files.write(destinationFile, decodedImg);}
					 */
					
					
			        Path filePath = uploadDirectory.resolve(imgName);		        			
			        //byte[] data = Base64.getDecoder().decode(medication.getImage());	
					//or
					//byte[] data = Base64.getDecoder().decode(inputStream.readAllBytes());
			        byte[] data = Base64.getDecoder().decode(medication.getImage().getBytes(StandardCharsets.UTF_8)); 
					FileOutputStream fileOutputStream = new FileOutputStream(filePath.toFile());
					fileOutputStream.write(data);
					
					fileOutputStream.close();
					
					
					medicationRepo.save(medication);
					dr.setMedicalLoadIds(dr.getMedicalLoadIds() + medication.getId()+","); // added
					dr.setState(State.LOADED);
					droneRepo.save(dr);
					Loads l = new Loads();
					l.setMedicationId(medication.getId());
					l.setMedicationWeight(medication.getWeight());
					l.setDroneId(dr.getId());
					l.setDroneWeight(dr.getWeightLimit());
				    loadsRepo.save(l);					
					message= "Medication has been successfully loaded on Drone. <br/> Download link to Medication picture: <a href='http://localhost:8080/medication_pic/" +imgName +"'> http://localhost:8080/medication_pic/" +imgName + "</a>";
					break;
				}
			}
	
		}		
		response.add(message);
		return response;
	}
	
	
	
	
	@GetMapping(value = "/view_all_drones")//get all drones
	public List<Drone> getAllDrones() {
		List<Drone> list = droneRepo.findAll();
		return list;
	}
	
	
	@GetMapping(value = "/check_loaded_medications/{droneId}")// checking loaded medication items for a given drone using the drone's id
	public List<Medication> checkLoad(@PathVariable long droneId) {
		List<Loads> loads = loadsRepo.findAll();
		List<Long> medicationIds = new ArrayList<>();
		for(Loads l : loads) {
			if(l.getDroneId() == droneId) {
				medicationIds.add(l.getMedicationId());
			}
		}
		List<Medication> meds = new ArrayList<>();
		for(long m : medicationIds) {
			meds.add(medicationRepo.findById(m).get());
		}
		return meds;
	}
	
	
	
	@GetMapping(value = "/available_drones")//get available drones for loading
	public List<Drone> getAvailableDrones() {
		List<Drone> list = droneRepo.findAll();
		List<Drone> available = new ArrayList<>();
		
		for(Drone d : list) {
			if((d.getBattery() >= 25) && (d.getState() == State.LOADING )) {
				available.add(d);
			}
		}
		return available;
	}
	
	
	@GetMapping(value = "/check_battery/{id}")//check a drone's battery level
	public List<String> checkDroneBattery(@PathVariable long id) {
		List<String> response = new ArrayList<>();
		
		Drone drone = null;
		try {
		drone = droneRepo.findById(id).get();
		}
		catch(Exception e) {
			response.add(e.getMessage());
			return response;
		}
		response.add("Battery Level for drone with id: " + id + " is "+ drone.getBattery() + "%");
		return response;
	}
	
	
	@GetMapping(value = "/check_all_drone_battery")//check a drone's battery level
	public List<String> checkAllDroneBattery() throws IOException{
		List<String> response = new ArrayList<>();
		
		List<Drone> drones = droneRepo.findAll();
		
		//File fileName = new File("Battery.txt");
		
        Path logDirectory = Paths.get("src/main/resources/static/battery_log");
        Path filePath = logDirectory.resolve("battery.log");
        
		FileWriter fileWriter = new FileWriter(filePath.toFile(), true);
		
		//LocalDateTime ldt = LocalDateTime.now();
		//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS");
		//System.out.println(ldt.format(formatter) + " Started thread " + name);
		
		for(Drone d : drones) {
			fileWriter.append(Calendar.getInstance().getTime().toString() +" - Battery Level for drone with id: " + d.getId() + " is "+ d.getBattery() + "% \r"); // for custom battery.log in logs folder
			logger.info("Battery Level for drone with id: " + d.getId() + " is "+ d.getBattery());// for spring LoggerFactory
			response.add(Calendar.getInstance().getTime().toString() +" - Battery Level for drone with id: " + d.getId() + " is "+ d.getBattery() + "%");
		}
		
		fileWriter.append("................................................................................\n"); // for custom battery.log in logs folder
	
		
		
		fileWriter.close();
		//return  "Battery Level for drone with id: " + id + " is "+ drone.getBattery() + "%";
		return response;
	}
	
	
	@GetMapping(value = "/update_drone_battery/{id}")//check a drone's battery level
	public List<String> updateDroneBattery(@PathVariable long id, @RequestParam("level") int level) {
		List<String> response = new ArrayList<>();
		
		Drone droneToUpdate = null;
		try {
	     droneToUpdate = droneRepo.findById(id).get();
		}
		catch(Exception e) {
			response.add(e.getMessage());
			return response;
		}
		
		if(level < 25) {
			droneToUpdate.setBattery(100);
			droneToUpdate.setState(State.IDLE);
			droneRepo.save(droneToUpdate);
			response.add("Battery level should not be above 100% - Battery Level for drone with id: " + id + " has been set to 100%");
		}
		else if(level > 100) {
			droneToUpdate.setBattery(100);
			droneRepo.save(droneToUpdate);
			response.add("Battery level should not be above 100% - Battery Level for drone with id: " + id + " has been set to 100%");
		}
		else {
		droneToUpdate.setBattery(level);
		droneRepo.save(droneToUpdate);
		response.add("Battery Level for drone with id: " + id + " has been set to "+ level + "%");
		}
		return response;
	}
	
	
	@GetMapping(value = "/update_drone_state/{id}")//check a drone's battery level
	public List<String> updateDroneBattery(@PathVariable long id, @RequestParam("state") State state) {
	
		List<String> response = new ArrayList<>();
		Drone droneToUpdate = null;
		try {
		 droneToUpdate = droneRepo.findById(id).get();
		}
		catch(Exception e) {
			response.add(e.getMessage());
			return response;
		}
		
		//IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;	
		
			if((state == State.IDLE) || (state == State.LOADING)  || (state == State.LOADED)  || (state == State.DELIVERING)  || (state == State.DELIVERED)  || (state == State.RETURNING)){
				droneToUpdate.setState(state);
				droneRepo.save(droneToUpdate);				
				response.add("The State for drone with id: " + id + " has been set to "+ state);
			}
			else {		
				response.add("Drone state not Updated - State can only be one of the following; IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;");
				
			}
			return response;
	}
		
	
}

