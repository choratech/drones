package com.drone.transport.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;



@Entity
public class Medication {
	/*
	 * - name (allowed only letters, numbers, ‘-‘, ‘_’); 
	 * - weight; 
	 * - code (allowed only upper case letters, underscore and numbers);
	 *  - image (picture of the medication case).
	 */
   
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id; //unique ID of medication load
	
	@Column
	private String name;
	
	@Column
	private double weight;
	
	@Column
	private String code;
	
	//@Column(name = "image", columnDefinition = "varchar(1000)")
	@Column
	@Lob
	private String image; // the image of the medication will be saved on the server, and the base64 encode will be sent along with other fields in JSON request body
   

	@Transient
	public static char[] nameConstraint = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', '0', '-', '_' };

	
	@Transient
	public static char[] codeConstraint = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
			'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '_' };
	
	
	
	
	 public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
    /* 	boolean check = false;
		char[] nameChar = name.toCharArray();
		for (int i = 0; i < nameChar.length; i++) {
			for (char s : nameConstraint) {
				if (nameChar[i] == s) {
					check = true;
				}
			}
			if (check == false) {
				System.out.println(name + " not allowed! Only letters, numbers, ‘-‘, ‘_’ are allowed");			
				this.name = getAlphaNumericString(nameConstraint, 10);
				System.out.println(this.name + " has been generated");
				break;
			}
			else {
				check = false;
			}
		}
		if (check == true) {
			this.name = name;
		}
		
		
	//this.name = getAlphaNumericString(nameConstraint, 10);
	 */
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
	/*	
		boolean check = false;
		char[] codeChar = code.toCharArray();
		for (int i = 0; i < codeChar.length; i++) {
			for (char s : codeConstraint) {
				if (codeChar[i] == s) {
					check = true;		
				}
			}
			if(check == false) {
				System.out.println(code + " not allowed! - Only upper case letters, underscore and numbers are allowed");
				this.code = getAlphaNumericString(codeConstraint, 10);
				System.out.println(this.code + " has been generated");
				break;
			}
			else {
				check = false;
			}
		}
		if (check == true) {
			this.code = code;
		}
		
		
		//this.code = getAlphaNumericString(codeConstraint, 10);		
		 * */
		this.code = code;
		 
	}
	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	
	private String getAlphaNumericString(char[] allowedCharacters, int neededCharacters)
	 {
	  
	  // create StringBuffer size of characters
	  StringBuilder sb = new StringBuilder(neededCharacters);
	 
	  for (int i = 0; i < neededCharacters; i++) {
	 
	   // generate a random number between
	   // 0 to allowedCharacters variable length
	   int index
	    = (int)(allowedCharacters.length * Math.random());
	 
	   // add Character one by one in end of sb
	   sb.append(allowedCharacters[index]);
	  }
	 
	  return sb.toString();
	 }



}
