package com.drone.transport.models;

public enum State {
	IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
}
