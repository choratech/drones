package com.drone.transport.models;

//import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
public class Drone{

	/*- serial number (100 characters max);
	- model (Lightweight, Middleweight, Cruiserweight, Heavyweight);
	- weight limit (500gr max);
	- battery capacity (percentage);
	- state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING).
	*/


//public enum State{
	//IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
//}

//public enum Model{
//	Lightweight, Middleweight, Cruiserweight, Heavyweight;
//	}

/*
 * enum Model{ Lightweight(350), Middleweight(400), Cruiserweight(450),
 * Heavyweight(500);
 * 
 * int capacity;
 * 
 * Model(int capacity){ this.capacity = capacity; } }
 */

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;	//unique iD of drone
	
	@Column
	private String serialNumber;
	
	@Column
	private double weightLimit;
	
	@Column
	private int battery;
	
	
	@Column
	private String medicalLoadIds; // used to hold the loaded medications (via the ids)
	
	
	//@Transient
	//private List<Medication> loader;
	
	
	//@Column(name = "state", columnDefinition = "varchar(255) default 'Lightweight'")
	@Column(name = "state")
	@Enumerated(EnumType.STRING)
	private State state;	
	
	
	//@Column(name = "model", columnDefinition = "varchar(255) default 'LOADING'")
	@Column(name = "model")
	@Enumerated(EnumType.STRING)
	private Model model;



//	@Column(name = "state", columnDefinition = "integer(10) default 0")
//	@Enumerated(EnumType.ORDINAL)
//	private State state;	
//	
//	
//	@Column(name = "model", columnDefinition = "integer(10) default 1")
//	@Enumerated(EnumType.ORDINAL)
//	private Model model;
	
	

	
	  public String getMedicalLoadIds() { return medicalLoadIds; }
	  
	  
	  public void setMedicalLoadIds(String medicalLoadIds) { this.medicalLoadIds =
	  medicalLoadIds; }
	  
	  
	 // public List<Medication> getLoader() { return loader; }
	  
	  
	 // public void setLoader(List<Medication> loader) { this.loader = loader; }
	 
	
	
	public long getId() {
		return id;
	}

	
	public void setId(long id) {
		this.id = id;
	}
	
    public String getSerialNumber() {
		return serialNumber;
	}
    
	public void setSerialNumber(String serialNumber) {	
		this.serialNumber = serialNumber;		
	}
	
	public double getWeightLimit() {
		return weightLimit;
	}
	
	public void setWeightLimit(double weightLimit) {
		this.weightLimit = weightLimit;
	}
	
	public int getBattery() {
		return battery;
	}
	public void setBattery(int battery) {
		//if(battery > 100) {
		//	battery = 100; //battery power should not be more than 100%, realistically
		//}
		this.battery = battery;
	}
	
	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
	
	
	/* public List<String> convertArrayToList(String[] input){
		List<String> output = new ArrayList<>();
		for(String s : input ) {
			output.add(s);
		}
		return output;
	}
	*/
	
	
	
}
