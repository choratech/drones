package com.drone.transport.models;

import javax.persistence.*;

@Entity
public class Loads {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long lno;
	
	@Column
    private long droneId;
	
	@Column
	private double droneWeight;
	
	@Column
	private long medicationId;
	
	@Column
	private double medicationWeight;
	
	

	public long getLno() {
		return lno;
	}

	public void setLno(long lno) {
		this.lno = lno;
	}

	public long getDroneId() {
		return droneId;
	}

	public void setDroneId(long droneId) {
		this.droneId = droneId;
	}

	public double getDroneWeight() {
		return droneWeight;
	}

	public void setDroneWeight(double droneWeight) {
		this.droneWeight = droneWeight;
	}

	public long getMedicationId() {
		return medicationId;
	}

	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}

	public double getMedicationWeight() {
		return medicationWeight;
	}

	public void setMedicationWeight(double medicationWeight) {
		this.medicationWeight = medicationWeight;
	}
	
	
	
	

	
	
	

}
