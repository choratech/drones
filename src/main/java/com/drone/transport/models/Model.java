package com.drone.transport.models;

public enum Model {
	Lightweight, Middleweight, Cruiserweight, Heavyweight;
}
