package com.drone.transport;

import java.io.FileWriter;
import java.net.ConnectException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.boot.SpringApplication;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/*import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
*/

//Threading scopes
//http://www.101coder.com/learnMultithreading
//https://medium.com/@kakurala/multi-threading-with-springboot-framework-b9fa84e37c3c

@Component
public class AsyncLogTask {
    
	@Async
	public void periodicLogWriter() {
	
		try {
			Thread.sleep(10000);	// Let me sleep for 10 sec
		
		     /*	try {	
				new Thread( new Runnable() {
					@Override
					public void run() {
                   //not used
					}

				}
				).join();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				*/
			
					  
			//clear Application Log file and start logging Drone battery levels
		    Path logDirectory = Paths.get("logs");
	        Path applicationLogPath = logDirectory.resolve("application.log");      
			FileWriter logWriter = new FileWriter(applicationLogPath.toFile());
			logWriter.write("");
			logWriter.close();
			
			
		HttpRequest getRequest = HttpRequest.newBuilder()
				.uri(new URI("http://localhost:8080/check_all_drone_battery"))
				//.headers("content-type", "application/json")
				.build();
		
	
		while(true) {
		HttpClient httpClient = HttpClient.newHttpClient();
		HttpResponse<String> getResponse = httpClient.send(getRequest, BodyHandlers.ofString());
		//System.out.println(postResponse.body());
		//String result = (String) new JSONObject(postResponse.body()).get("status");
		//System.out.println(result);
		String result = getResponse.body();
		Thread.sleep(10000); //make a call every 10 sec
		}
	
		} catch (InterruptedException e) {
			//e.printStackTrace();
			//logger.info(e.toString());
		}
		catch (ConnectException exc) {
			//e.printStackTrace();
			//logger.info(exc.toString());
		}
		catch (IllegalArgumentException exc) {
			//e.printStackTrace();
			//logger.info(exc.toString());
		}
		
		catch (Exception exc) {
			//e.printStackTrace();
			//logger.info(exc.toString());
		}
	
	}
	
}
