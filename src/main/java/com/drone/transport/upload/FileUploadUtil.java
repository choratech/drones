package com.drone.transport.upload;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.multipart.MultipartFile;


/*  This  class  is  not  used - ignore  */
public class FileUploadUtil {

	public static String saveFile(String fileName, MultipartFile multipartFile) throws IOException {
		Path uploadDirectory = Paths.get("medication_pictures");
		
		String fileCode = RandomStringUtils.randomAlphanumeric(8);
		
		try {
			InputStream inputStream = multipartFile.getInputStream();
			Path filePath = uploadDirectory.resolve(fileCode + "-" + fileName);
			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
		}
		catch(IOException io) {
			throw new IOException("Error saving uploaded Image: " + fileName, io);
		}
		
		return fileCode + "-" + fileName;
		
	}
	
	public static String encodeImage(String imgPath, String savePath) throws Exception {
		
		FileInputStream  imageStream = new FileInputStream(imgPath);
		byte[] data = imageStream.readAllBytes();
		
		String imageString = Base64.getEncoder().encodeToString(data);
		
		FileWriter fileWriter = new FileWriter(savePath);
		fileWriter.write(imageString);
		
		fileWriter.close();
		imageStream.close();
		
		return imageString;
		
	}
	
	public static void decodeImage(String txtPath, String savePath) throws Exception {
		
		FileInputStream inputStream = new FileInputStream(txtPath);
		
		byte[] data = Base64.getDecoder().decode(new String(inputStream.readAllBytes()));
		
		//or
		//byte[] data = Base64.getDecoder().decode(inputStream.readAllBytes());
		
		FileOutputStream fileOutputStream = new FileOutputStream(savePath);
		fileOutputStream.write(data);
		
		fileOutputStream.close();
		inputStream.close();
		
	}
}
